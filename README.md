# Layerfused X201

These are the models for the LayerFused X201 open source printer.

Models can be viewed directly on the Bitbucket site if you install the [File Viewer](https://marketplace.atlassian.com/apps/1214164/file-viewer-for-bitbucket-cloud) app.
